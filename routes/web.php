<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\PostController;

Route::prefix('admin')->group(function (){
    Route::get('/index/post',[PostController::class,'index'])->name('admin.dashboard');
    Route::get('/index/active',[PostController::class,'active'])->name('admin.post.active');
    Route::get('/index/inactive',[PostController::class,'inactive'])->name('admin.post.inactive');
    Route::get('/index/create',[PostController::class,'index'])->name('admin.post.create');

    Route::post('/index/post/create',[PostController::class,'store'])->name('admin.post.store');
    Route::get('/index/post/edit/{id}',[PostController::class,'edit'])->name('admin.post.edit');
    Route::get('/index/post/delete/{id}',[PostController::class,'destroy'])->name('admin.post.delete');
    Route::post('/index/post/create',[PostController::class,'store'])->name('admin.post.store');
    Route::post('/index/post/update/{id}',[PostController::class,'update'])->name('admin.post.update');
});
