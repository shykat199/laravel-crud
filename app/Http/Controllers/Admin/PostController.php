<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PostRequest;
use App\Models\Post;
use Faker\Core\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        return view('admin.blog.create');
    }


    public function active()
    {
        $activePost = Post::where('post_status', '=', 1)->paginate(2);
        //dd($activePost);
        return view('admin.blog.active', compact('activePost'));
    }

    public function inactive()
    {
        $inActivePost = Post::where('post_status', '=', 0)->paginate(3);
        return view('admin.blog.inactive', compact('inActivePost'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request)
    {
//        $request->validate([
//            'post_title'=>['required'],
//            'post_description'=>['required'],
//            'post_image'=>['required'],
//        ],[
//            'post_title.required'=>'Please Fill The Blog Title First.',
//            'post_description.required'=>'Please Fill The Blog Description .',
//            'post_image.required'=>'Please Select An Image For Your Blog',
//        ]);

        if ($request->hasFile('post_image')) {
            $name = random_int(111, 2000) . '.' . $request->file('post_image')->getClientOriginalExtension();
            $image = Storage::put('/public/post-images/' . $name, file_get_contents($request->file('post_image')));
        }
        $storePost = Post::create([
            'post_title' => $request->get('post_title'),
            'user_id' => 1,
            'post_description' => $request->get('post_description'),
            'post_image' => $name,
        ]);

        if ($storePost) {
            return 'Success';
        } else {
            return "Not Success";
        }


    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $post = Post::findOrFail($id);
        return view('admin.blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $post = Post::findOrFail($id);
        //dd($post);
        $upPost = $post->update([
            'post_title' => $request->get('post_title'),
            'post_description' => $request->get('post_description'),
        ]);

        if ($upPost) {
            return to_route('admin.post.active')->with('success', 'Post Updated Successfully');
        } else {
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //dd($id);
        $post = Post::findOrFail($id);
        //dd($post);
//        $image_path = ("/public/post-images/{$post->post_image}");
//        //dd($image_path);
//
//
//            //File::delete($image_path);
//            \Illuminate\Support\Facades\File::delete($image_path);

        $postDelt = $post->delete();

        if ($postDelt) {
            return to_route('admin.post.active')->with('success', "Post has been deleted");
        } else {
            return Redirect::back()->with('error', "Post can't be deleted");
        }

    }
}
