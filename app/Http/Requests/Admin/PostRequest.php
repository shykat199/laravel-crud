<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'post_title'=>['required'],
            'post_description'=>['required'],
            'post_image'=>['required'],
        ];
    }

    public function messages(): array
    {
        return[
            'post_title.required'=>'Please Fill The Blog Title First.',
            'post_description.required'=>'Please Fill The Blog Description .',
            'post_image.required'=>'Please Select An Image For Your Blog',
        ];
    }
}
