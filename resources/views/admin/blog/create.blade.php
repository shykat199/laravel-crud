@extends('admin.layout.admin_master')

@section('admin.content')
    <div class="sl-pagebody">
        <div class="card">
            <div class="card-header">
                Add New Post
            </div>
            <div class="card-body">
                <h5 class="card-title">Add New Post</h5>

                <form action="{{route('admin.post.store')}}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="form-group">
                        <label for="post_title">Blog Title</label>
                        <input type="text" name="post_title" class="form-control" id="post_title" aria-describedby="post_title" placeholder="Enter Post Title">
                    </div>
                    @error('post_title')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                    <div class="form-group">
                        <label for="post_description">Post Description</label>
                        <textarea class="form-control" name="post_description" id="post_description" rows="3"></textarea>

                    </div>
                    @error('post_description')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputPassword1">Post Image</label>
                        <input type="file" name="post_image" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    @error('post_image')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
@endsection
