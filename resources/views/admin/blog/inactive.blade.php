@extends('admin.layout.admin_master')

@section('admin.content')
    <div class="sl-pagebody">
        <div class="card">
            <div class="card-header">
                All Active Post
            </div>
            <div class="card-body">
                <h5 class="card-title">All Active Post</h5>
                <div class="div">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#Id</th>
                            <th scope="col">Post Title</th>
                            <th scope="col">Post Description</th>
                            <th scope="col">Post Status</th>
                            <th scope="col">Post Image</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $idx=1;
                        @endphp
                        @foreach($inActivePost as $post)
                            <tr>
                                <th scope="row">{{$idx++}}</th>
                                <td>{{$post->post_title}}</td>
                                <td>{{\Illuminate\Support\Str::limit($post->post_description,100,'...')}}</td>
                                <td>
                                    @if($post->post_status==0)
                                        <h6> <span class="badge badge-danger">Inactive</span></h6>
                                    @else
                                        <h6> <span class="badge badge-success">Active</span></h6>
                                    @endif

                                </td>
                                <td>
                                    <img src="{{asset('storage/post-images/'.$post->post_image)}}" alt="" style="height: 100px; width: auto">
                                </td>
                                <td>
                                    <div style="display: flex">
                                        <a href="" class="btn btn-warning mr-2">Edit</a>
                                        <a href="" class="btn btn-danger">Delete</a>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    @if ($inActivePost->hasPages())
                        <div class="pagination-wrapper">
                            {{ $inActivePost->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
